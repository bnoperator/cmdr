library(bnutil)
library(shiny)

bnMessageHandler = bnshiny::BNMessageHandler$new()
bnshiny::startBNTestShiny('cmdr', sessionType='run', bnMessageHandler=bnMessageHandler)
